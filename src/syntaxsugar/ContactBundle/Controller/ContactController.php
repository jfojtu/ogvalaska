<?php

namespace syntaxsugar\ContactBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use syntaxsugar\ContactBundle\Entity\Enquiry;
use syntaxsugar\ContactBundle\Form\EnquiryType;

class ContactController extends Controller
{
    public function indexAction()
    {
        return $this->render('syntaxsugarContactBundle:Contact:index.html.twig');
    }

    public function contactAction(Request $request)
    {
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Contact enquiry from ogvalaska.cz')
                ->setFrom('enquiries@ogvalaska.cz')
                ->setTo('jaromir.fojtu@gmail.com')
                ->setBody($this->renderView('syntaxsugarContactBundle:Contact:contactEmail.txt.twig', array('enquiry' => $enquiry)));
            $this->get('mailer')->send($message);

            $this->addFlash(
                'contact-notice',
                'Zpráva byla odeslána!'
            );

            return $this->redirect($this->generateUrl('syntaxsugar_contact_contact'));
        }

        return $this->render('syntaxsugarContactBundle:Contact:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
