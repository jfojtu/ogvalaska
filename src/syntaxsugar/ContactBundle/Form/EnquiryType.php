<?php

namespace syntaxsugar\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EnquiryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array('label' => 'Jméno'));
        $builder->add('email', 'email', array('label' => 'Email'));
        $builder->add('body', 'textarea', array('label' => 'Zpráva'));
    }

    public function getName()
    {
        return 'contact';
    }
}